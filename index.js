const server = require('http').createServer();
const io = require('socket.io')(server,{
    pingInterval: 10000,
    pingTimeout: 5000,
    cookies: false
});
var users = [];

server.listen(6677,() => {
    console.log("El servidor esta funcionando en localhost:6677");
});

io.on('connection',(socket) => {
    console.log('Nuevo cliente conectado ' + socket.handshake.address + ', id: ' + socket.id);

    // Evento cuando un usuario se conecta al servicio
    /*
    * Se debe enviar al usuario los demas usuarios registrados
    * luego se debe registrar al usuario y enviar los datos del usuario a los demas usuarios registrados
    * */
    socket.on('onConnect', (data) => {
        // console.log(data);
        data.socketId = socket.id;
        socket.emit('getUsers',users);
        users.push(data);
        io.emit('newUser', data, socket.id, );

    });

    // Evento cuando un usuario se desconecta del servicio
    socket.on('disconnect',() => {
        users.forEach((value, index) => {
            if (value.socketId === socket.id){
                users.splice(index, 1);
            }
        })
    });

    // Evento cuando un usuario se conecta a un grupo
    socket.on('joinGroup',(data) =>{
        socket.join(data.roomName);
    });

    // Evento cuando un usuario deja un grupo
    socket.on('leaveGroup', (data) => {
        socket.leave(data.roomName);
    });

    // Evento cuando un usuario envia un mensaje a otro usuario por privado
    socket.on('sendMessage', (data) =>{
        socket.to(data.socketId).emit('onNewPrivateMessage',data.message);
        //console.log(data);
    });

    // Evento cuando un usuario envia un mensaje a rsun grupo
    socket.on('sendGroupMessage', (data) => {
        socket.to(data.roomName).emit(data.message);
    });

    // Evento cuando un usuario envia un mensaje a
});